import gpxpy
import pandas as pd
import matplotlib.pyplot as plt

def GPXpars(file):
    gpx_file = open(file, 'r')
    gpx = gpxpy.parse(gpx_file)
    data = gpx.tracks [0] .segments [0] .points
    df = pd.DataFrame(columns=['lon', 'lat', 'alt', 'time'])
    for point in data:
        df = df.append({'lon': point.longitude, 
                        'lat' : point.latitude, 
                        'alt' : point.elevation, 
                        'time' : point.time},
                        ignore_index=True)
    return df

df = GPXpars('AlysisMeth/track.gpx')
plt.plot(df['lon'], df['lat'])
