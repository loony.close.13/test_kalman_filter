from math import cos, sin, sqrt
from typing import List, Tuple
from random import randint

class Accelerometer():
    def __init__(self):
        self.amount_points: int = 15                            # кол-во точек
        self.t_GPS: float = 1.0                                 # сек
        self.t_accel: float = 0.01                              # сек
        self.speed_const: float = 60                            # км/ч
        self.t_max_spid: float = randint(1,5)                              # сек
        self.accel: float = self.speed_const/self.t_max_spid    # км/(ч^2) 
        self.c: int = int(self.speed_const/self.t_accel)
    
    
    # генерируем точки
    def point_generate(self) -> Tuple[List[float], List[float]]: 
        
        x: List[float] = [0.0]
        y: List[float] = [0.0]
        lens_x: float = 0.0
        lens_y: float = 0.0
        
        for i in range(0, self.amount_points):
            lens_x += randint(45,1000)
            lens_y += randint(45,1000)
            x.append(lens_x)
            y.append(lens_y)
        return x, y 

    # вычисляем длину отрезка
    def segment_length(self, x: List[float], y: List[float]) -> List[float]: 
        len_L: List[float] = []
        for i in range(1, len(x)):
            L = sqrt((x[i]-x[i-1])**2 + (y[i]+y[i-1])**2)
            len_L.append(L)
        return len_L

    # длина пройденного пути
    def distance(self, len_L: float)-> List[float]:
        speed: List[float] = [0.0]
        l_accel: List[float] = [0.0]
        l_speed: List[float] = [0.0]
        for n in range(0, self.c):
            speed_point = speed[n] + self.accel*self.t_accel
            speed.append(speed_point)
            if speed_point >= self.speed_const:     
                break
            l_accel.append(speed[n]*self.t_accel+(self.accel*self.t_accel**2)/2)

        l_speed_const= len_L-sum(l_accel)*2
        i = l_speed_const/self.speed_const/self.t_accel
        for n in range(0, int(i)):
            l_speed.append(self.speed_const*self.t_accel)  
        
        
        l_all: List[float] = []
        for i in range(1, len(l_accel)):
            l_accel[i] = l_accel[i]+l_accel[i-1]
        l_all.extend(l_accel)

        l_speed[0] = l_speed[0] + l_all[-1]
        for i in range(1, len(l_speed)):
            l_speed[i] = l_speed[i] + l_speed[i-1]
        l_all.extend(l_speed)

        l3: List[float] = l_accel[::-1]
        l3[0] = l3[0] + l_speed[-1]
        for i in range(1, len(l3)):
            l3[i] = l3[i]+l3[i-1]
        l_all.extend(l3)
        
        return l_all

    # находим лямбду
    def lambda_list(self, L: float, l_all: List[float]) -> List[float]:
        lambda_all: List[float] = []
        for n in range(1, len(l_all)): lambda_all.append(L/l_all[n])
        #for i in range(1, len(lambda_all)): lambda_all[i] = lambda_all[i] + lambda_all[i-1]
        return lambda_all
    
    # координаты точек
    def point_coord(self, x0: float, x1: float, y0: float, y1: float, lambda_add: List[float]) -> Tuple[List[float], List[float]]:
        x_n: List[float] = [0.0]
        y_n: List[float] = [0.0]
        for n in range(0, len(lambda_add)):
            x_n.append((x0 + x1 *(lambda_add[n]))/(1+lambda_add[n]))
            y_n.append((y0 + y1 *(lambda_add[n]))/(1+lambda_add[n]))
        return x_n, y_n

    # ускорение 
    def point_accel(self, x: List[float], y:List[float]) -> Tuple[List[float], List[float]]:
        accel_x: List[float] = [0.0]
        accel_y: List[float] = [0.0]
        for n in range(1, len(x)):
            accel_x.append(self.accel*cos(x[n]-x[n-1]))
            accel_y.append(self.accel*sin(y[n]-y[n-1]))
        return accel_x, accel_y
    
    def activate_acc(self) -> Tuple[List[float], List[float]]:
        xy = self.point_generate()       
        x = xy[0]
        y = xy[1]
        
        len_L = self.segment_length(x, y)
        accel_xy: Tuple[List[float],List[float]] = ([],[])
        lambda_all = []
        for L in len_L:
            l_all = self.distance(L) 
            lambda_all.append(self.lambda_list(L, l_all))
        
        xy_n: Tuple[List[float],List[float]] = ([],[])
        x_n: List[float] = []
        y_n: List[float] = []

        for i in range(0, len(x)-1):
            xy_n = self.point_coord(x[i], x[i+1], y[i], y[i+1], lambda_all[i])
            x_n.extend(xy_n[0])
            y_n.extend(xy_n[1])
        accel_xy = self.point_accel(x_n, y_n)

        return accel_xy

# A = Accelerometer()
# xy = A.activate()
# print(xy)
