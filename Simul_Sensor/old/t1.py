import DictMap as dm
from random import randint

amount_points = 15

dict_generate = dm.Map([{'namber_line': None, 'coords': [{'x': None,'y':None}],'segment_length': None, 'point': []}])
lens_x: float = 0.0
lens_y: float = 0.0
counter_number: int = 0

for i in range(0, amount_points):
    counter_number += 1
    lens_x += randint(30,1000)
    lens_y += randint(30,1000)
            
    dict_generate[-1]['number_line'].append(counter_number)
    dict_generate[-1]['coords'][-1]['x'].append(lens_x)
    dict_generate[-1]['coords'][-1]['y'].append(lens_y)