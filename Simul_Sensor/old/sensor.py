import csv

def Sensor(csv_reader):
    results = []
    with open(csv_reader, newline='') as File:  
        reader = csv.reader(File)
        for row in reader:
            results.append(row)
    
    return results[1:]
    