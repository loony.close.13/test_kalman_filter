# include <assert.h>
# include <stdlib.h>

# include "GpsAccFusionFilter.hpp"
# include "Matrix.hpp"
# include "Kalman.hpp"

from typing import List, Tuple
import numpy as np
import numpy.typing as npt

# GPSAccFusionFilter::GPSAccFusionFilter(const FusionFilterState &init_state,
#                                        double acc_deviation,
#                                        double pos_deviation,
#                                        double last_predict_ms) :
#   m_last_predict_ms(last_predict_ms),
#   m_acc_deviation(acc_deviation),
#   m_current_state(init_state),
#   m_predicts_count(0) {
#   Xk_k.set({
#              init_state.x,
#              init_state.y,
#              init_state.x_vel,
#              init_state.y_vel
#            });

#   H = Matrix<double, measure_dim, state_dim>::Identity();
#   Pk_k = Matrix<double, state_dim, state_dim>::Identity();
#   Pk_k *= pos_deviation;
# }
# //////////////////////////////////////////////////////////////
# struct FusionFilterState {
#   double x; // longitude in meters
#   double y; // latitude in meters
#   double x_vel;
#   double y_vel;

#   FusionFilterState() : x(0.0), y(0.0), x_vel(0.0), y_vel(0.0) {}
#   FusionFilterState(double x_, double y_,
#                     double x_vel_, double y_vel_) :
#     x(x_), y(y_), x_vel(x_vel_), y_vel(y_vel_) {}
# };

class GPSAccFusionFilter():
    def __init__(self, init_state, acc_deviation, pos_deviation, last_predict_ms) -> None:
        self.m_last_predict_ms =last_predict_ms
        self.m_acc_deviation = acc_deviation
        self.m_predicts_count = 0
        
        self.Xk_k = np.array([ x, y, x_vel, y_vel])

# void
# GPSAccFusionFilter::predict(double xAcc,
#                             double yAcc,
#                             double time_ms) {

#   double dt_sec = (time_ms - m_last_predict_ms) / 1.0e+3; //ms to sec. cause we use m/sec, m/sec^2 etc.
#   rebuild_F(dt_sec);
#   rebuild_B(dt_sec);
#   rebuild_U(xAcc, yAcc);
#   ++m_predicts_count; // todo check for overflow
#   rebuild_Q(m_acc_deviation);
#   m_last_predict_ms = time_ms;
#   estimate();
#   Xk_km1 = Xk_k;
# }
# //////////////////////////////////////////////////////////////

    def predict_X(self, xAcc, yAcc, time_ms):
        dt_sec = (time_ms - self.m_last_predict_ms) / 1.0e+3 
        self.rebuild_F(dt_sec)
        self.rebuild_B(dt_sec)
        self.rebuild_U(xAcc, yAcc)
        self.m_predicts_count += self.m_predicts_count
        self.rebuild_Q(self.m_acc_deviation)
        m_last_predict_ms = time_ms
        #estimate()
        Xk_km1 = self.Xk_k
        return Xk_km1

# void
# GPSAccFusionFilter::update(const FusionFilterState &state,
#                            double pos_deviation) {
#   m_predicts_count = 0;
#   rebuild_R(pos_deviation);
#   Zk.set({
#            state.x,
#            state.y,
#            state.x_vel,
#            state.y_vel
#          });
#   correct();
# }
# //////////////////////////////////////////////////////////////

    def update(self, pos_deviation, x, y, x_vel, y_vel):
        m_predicts_count = 0
        self.rebuild_R(pos_deviation)
        Zk = np.array([ x, y, x_vel, y_vel])
        #correct()
        return Zk

    def rebuild_F(self, dt_ms: float):
        F = np.array([[1.0, 0.0, dt_ms, 0.0], 
                    [0.0, 1.0, 0.0, dt_ms],
                    [0.0, 0.0, 1.0, 0.0], 
                    [0.0, 0.0, 0.0, 1.0]])
        return F

    def rebuild_U(self, xAcc: float, yAcc: float):
        Uk = np.array([xAcc, yAcc])
        return Uk

    def rebuild_B(self, dt_ms: float):
        dt_2: float = 0.5 * dt_ms ** 2
        B = np.array([[dt_2, 0.0],
                    [0.0, dt_2],
                    [dt_ms, 0.0],
                    [0.0, dt_ms]])
        return B

    def rebuild_Q(self, acc_deviation:float):
        vel_dev = acc_deviation * self.m_predicts_count
        pos_dev = vel_dev * self.m_predicts_count
        cov_dev = vel_dev *pos_dev

        pos_dev_2 = pos_dev * pos_dev
        vel_dev_2 = vel_dev * vel_dev

        Q = np.array([[pos_dev_2, 0.0, cov_dev, 0.0],
                    [0.0, pos_dev_2, 0.0, cov_dev],
                    [cov_dev, 0.0, pos_dev_2, 0.0],
                    [0.0, cov_dev, 0.0, pos_dev_2]])

    def rebuild_R(self, pos_sigma: float):
        vel_sigma: float = pos_sigma * 1.0e-01
        R = np.array([[pos_sigma, 0.0, 0.0, 0.0],
                    [0.0, pos_sigma, 0.0, 0.0],
                    [0.0, 0.0, vel_sigma, 0.0],
                    [0.0, 0.0, 0.0, vel_sigma]])
        return R