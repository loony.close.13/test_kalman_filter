from collections import namedtuple
from math import cos, sin, sqrt
from typing import List, Tuple
from random import randint

class Accelerometer():
    def __init__(self):
        self.amount_points: int = 15                                        # кол-во точек
        self.t_GPS: float = 1.0                                             # сек
        self.t_accel: float = 0.01                                          # сек
        self.speed_const: float = 60                                        # км/ч

        self.t_accel_msec: float = self.t_accel*100                         # msec                         
        self.speed_ms: float = self.speed_const*1000/3600                   # m/s
        self.speed_mms: float = self.speed_const*1000/3600*100              # m/ms
        self.t_max_speed: float = randint(1,5)                              # sec
        self.t_max_speed_msec: float = self.t_max_speed*100                 # ms
        self.accel: float = self.speed_mms/self.t_max_speed                 # m/(s^2) 
        self.l_acc_max: float = (self.accel*self.t_max_speed_msec**2)/2     #m

        self.c: int = int(self.speed_const/self.t_accel)

        self.speed: List[float] = [0.0]

    # генерируем точки
    def __point_generate(self) -> None: 
        
        self.x: List[float] = [0.0]
        self.y: List[float] = [0.0]
        lens_x: float = 0.0
        lens_y: float = 0.0
        
        for i in range(0, self.amount_points):
            lens_x += randint(45,1000)
            lens_y += randint(45,1000)
            self.x.append(lens_x)
            self.y.append(lens_y)
         

    # вычисляем длину отрезка
    def __segment_length(self) -> None: 
        self.len_L: List[float] = []
        for i in range(1, len(self.x)):
            L = sqrt((self.x[i]-self.x[i-1])**2 + (self.y[i]+self.y[i-1])**2)
            self.len_L.append(L)
        

    def __L_accel(self) -> None:
        self.speed_acc: List[float] = [0,0]
        self.l_accel: List[float] = [0.0]
        c: int = 0
        while self.speed_const <= self.speed_acc[c]:
            speed_point = self.speed[c] + self.accel*self.t_accel_msec                               
            self.speed_acc.append(speed_point)
            self.l_accel.append(self.speed_acc[c]*self.t_accel_msec+(self.accel*self.t_accel_msec**2)/2)    
            self.speed.append(speed_point)
            c += 1
            
    
    # длина пройденного пути
    def __distance(self)-> None:
        self.l_all: List[float] = []
        l_speed: List[float] = []
        for i in range(0, len(self.len_L)):
            l_speed_const= self.len_L[i]-sum(self.l_accel)**2
            c = l_speed_const/self.speed_mms/self.t_accel_msec
            for n in range(0, int(c)):
                l_speed.append(self.speed_const*self.t_accel) 
        
        self.l_all.extend(self.l_accel)
        self.l_all.extend(l_speed)
        self.l_all.extend(self.l_accel[::-1])
        for i in range(1, len(self.l_all)):
            self.l_all[i] += self.l_all[i-1]


    # находим лямбду
    def __lambda_list(self, L: float, l_all: List[float]) -> None:
        self.lambda_all: List[float] = []
        for n in range(1, len(l_all)): self.lambda_all.append(L/l_all[n])
        
    
    # координаты точек
    def __point_coord(self, x0: float, x1: float, y0: float, y1: float,) -> None:
        self.x_n: List[float] = [0.0]
        self.y_n: List[float] = [0.0]
        for n in range(0, len(self.lambda_all)):
            self.x_n.append((x0 + x1 *(self.lambda_all[n]))/(1+self.lambda_all[n]))
            self.y_n.append((y0 + y1 *(self.lambda_all[n]))/(1+self.lambda_all[n]))
        

    # ускорение 
    def __point_accel(self) -> None:
        self.accel_x: List[float] = [0.0]
        self.accel_y: List[float] = [0.0]
        for n in range(1, len(self.x_n)):
            self.accel_x.append(self.accel*cos(self.x_n[n]-self.x_n[n-1]))
            self.accel_y.append(self.accel*sin(self.y_n[n]-self.y_n[n-1]))
        

    def activate(self) -> dict:
        data = {}

        len_point = len(self.l_all)
        for n in range(0, len_point):
            data1 = ({'point':[{'number_point': n,
                                'coord':{'x': self.x_n[n], 'y': self.x_n[n]},
                                'accel':{'x': self.accel_x[n], 'y': self.accel_y[n]},
                                'speed': self.speed[n]
                              }]
                     })
            data[-1].append(data1)
        
        return data

A = Accelerometer()
d = A.activate()
print(d)


