# Test Kalman Filter
#### Данный репозиторий создан с целью создать фильтра калмана типов:
- [EKF](https://gitlab.com/loony.close.13/test_kalman_filter/-/blob/master/Jupyter_test_KF/Test_KF.ipynb) - Extended Kalman Filter
- [UKF](https://gitlab.com/loony.close.13/test_kalman_filter/-/blob/master/Jupyter_test_KF/Test_UKF.ipynb) - Unscented Kalman Filter
#### И реализации фильтрации с помощью его данных GPS и акселерометра для построения правдоподобной траектории по данным.
#### Для этого:
- [Создаю карту из картинки построить функцию построения траектории](https://gitlab.com/loony.close.13/test_kalman_filter/-/blob/master/Maps/Maps.ipynb) (не реализовано)
- [Создать имитатор датчиков для фильтрации](https://gitlab.com/loony.close.13/test_kalman_filter/-/blob/master/Simul_Sensor/sensor.py) (пока что простейший GPS)
- Создать после фильтрации ФК траекторию на карте в реальном времени с датчика. (не реализован)
- Анализ сравнения и выводы возможные оптимизации в будущем (не реализовано)

#### Все остальные файлы были созданы в качестве разбора и обучения.
 
 



